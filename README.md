gray-monarch.freeswitch
=========

Ansible role to install the default binary packages of Freeswitch on:

-   CentOS 7
-   Debian 8

Requirements
------------

None

Role Variables
--------------

##### defaults/main.yml

|                       Variable Name | Description              | Default                                          |
|------------------------------------:|:-------------------------|:-------------------------------------------------|
|      `gm_fs_freeswitch_switch_conf` | switch.conf XML location | /etc/freeswitch/autoload_configs/switch.conf.xml |
|               `gm_fs_odbc_do_setup` | ODBC Setup boolean       | false                                            |
|      `gm_fs_odbc_freeswitch_driver` | ODBC driver name         | MySQL                                            |
|         `gm_fs_odbc_freeswitch_dsn` | ODBC DSN name            | freeswitch                                       |
|      `gm_fs_odbc_freeswitch_server` | DSN host                 | localhost                                        |
| `gm_fs_odbc_freeswitch_server_port` | DSN port                 | 3306                                             |
|     `gm_fs_odbc_freeswitch_db_user` | DSN DB user              | freeswitch                                       |
|     `gm_fs_odbc_freeswitch_db_name` | DSN DB name              | freeswitch                                       |
|     `gm_fs_odbc_freeswitch_db_pass` | DSN DB password          | test123                                          |
|      `gm_fs_odbc_freeswitch_socket` | DSN socket               | /var/lib/mysql/mysql.sock                        |
|       `gm_fs_odbc_freeswitch_trace` | DSN trace option         | Yes                                              |
|  `gm_fs_odbc_freeswitch_trace_file` | DSN trace file           | /tmp/mysql.odbc                                  |

##### vars/main.yml

|                 Variable Name | Description                        | Default    |
|------------------------------:|:-----------------------------------|:-----------|
| gm_fs_freeswitch_service_name | The name of the freeswitch service | freeswitch |

##### vars/Debian.yml

|            Variable Name | Description                         | Default                                                                  |
|-------------------------:|:------------------------------------|:-------------------------------------------------------------------------|
| gm_fs_freeswitch_pgp_key | URL that points to FS repo PGP key  | <https://files.freeswitch.org/repo/deb/debian/freeswitch_archive_g0.pub> |
|    gm_gs_freeswitch_repo | URL string of Debian-formatted repo | deb <http://files.freeswitch.org/repo/deb/freeswitch-1.6/> jessie main   |
|    gm_fs_freeswitch_pkgs | A list of packages to install       | - freeswitch-meta-all                                                    |

##### vars/RedHat.yml

|         Variable Name | Description                                   | Default                                                                       |
|----------------------:|:----------------------------------------------|:------------------------------------------------------------------------------|
| gm_fs_freeswitch_repo | RPM **package** of Freeswitch repo for RHEL   | <http://files.freeswitch.org/freeswitch-release-1-6.noarch.rpm>               |
| gm_fs_freeswitch_pkgs | List of packages to install through yum       | - freeswitch-config-vanilla<br>- freeswitch-lang-\*<br>- freeswitch-sounds-\* |
| gm_fs_dependency_pkgs | List of packages to install before Freeswitch | - epel-release                                                                |

Dependencies
------------

~~A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.~~

Example Playbook
----------------

~~Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:~~

    - hosts: servers
      roles:
         - gray-monarch.freeswitch

License
-------

Apache 2

Author Information
------------------

Michael Goodwin <mike@contactvelocity.com>
